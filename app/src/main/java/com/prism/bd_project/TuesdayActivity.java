package com.prism.bd_project;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.sql.SQLException;

/**
 * Created by New on 19.05.2015.
 */
public class TuesdayActivity extends Activity {

    private ArrayAdapter<String> adapter;
    public static ListView listViewTuesday;
    Cursor cursor;
    DbAdapter dbAdapter;
    String[] channels;
    String channelName;
    String DayOfWeek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tuesday_layout);

        listViewTuesday = (ListView) findViewById(R.id.listViewTuesday);

        dbAdapter = new DbAdapter(getApplicationContext());
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        channelName = getIntent().getStringExtra("channelName");
        DayOfWeek = DateFormatCustom.getDate().get("Tuesday");
        Log.i("IntentTuesday", channelName);
        cursor = dbAdapter.getProgramByDate(channelName, DayOfWeek);
        channels = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int i = 0; i < channels.length; i++) {
            channels[i] = cursor.getString(cursor.getColumnIndex(DbHelper.PROGRAM_TIME))
                    + "  " + cursor.getString(cursor.getColumnIndex(DbHelper.PROGRAM_NAME));
            cursor.moveToNext();
        }
        if(channels.length < 1){
            channels = new String[]{"На этот день нет расписания"};
        }
        cursor.close();
        if(listViewTuesday == null){Log.i("IntentTuesday", "бляяя");}
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, channels);
        listViewTuesday.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbAdapter.close();
    }
}
