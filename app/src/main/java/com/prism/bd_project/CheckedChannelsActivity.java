package com.prism.bd_project;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by New on 18.05.2015.
 */
public class CheckedChannelsActivity extends ActionBarActivity implements View.OnClickListener, SearchView.OnQueryTextListener{

    private ArrayAdapter<String> adapter;
    ListView listView;
    Cursor cursor;
    DbAdapter dbAdapter;
    String[] channels;
    parsing pars = new parsing();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checked_channels);

        listView = (ListView) findViewById(R.id.listView);

        dbAdapter = new DbAdapter(getApplicationContext());
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        cursor = dbAdapter.getProgramLoad();
        channels = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int i = 0; i < channels.length; i++) {
            channels[i] = cursor.getString(cursor.getColumnIndex(DbHelper.CHANNEL_NAME));
            cursor.moveToNext();
        }
        cursor.close();

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, channels);
        listView.setAdapter(adapter);

        //короткое нажатие на элемент списка
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Intent intentTab = new Intent(getApplicationContext(), );
                Intent intent = new Intent(getApplicationContext(), ProgramActivity.class);
                intent.putExtra("channelName", adapter.getItem(position));
                Log.i("MyTag", adapter.getItem(position));

                startActivity(intent);
            }
        });

        //долгое нажатие на элемент списка
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("MyTag", "долгое нажатие " + adapter.getItem(position));
                final String pos = adapter.getItem(position);


                AlertDialog.Builder builder = new AlertDialog.Builder(CheckedChannelsActivity.this);

                //задаем параметры
                builder.setTitle("Предупреждение")  //заголовок
                        .setMessage("Вы действительно хотите удалить канал")
                        .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(), "Отмена", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Toast.makeText(getApplicationContext(), "Удалено", Toast.LENGTH_SHORT).show();
                                dbAdapter.deleteChannel(pos);
                                ////////////////////
                                reloadListView();

                            }
                        })
                        .setCancelable(false) //можно ли вернуться кнопкой назад
                        ;

                AlertDialog alert = builder.create();
                alert.show();


                return true;
            }
        });
    }

    public void tabClick(View view){
        Intent intent = new Intent(this, ProgramActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("Поиск");
        searchView.setOnQueryTextListener((SearchView.OnQueryTextListener) this);

        setTitle("Управление каналами");

        return true;
    }

    //обработка нажатий на пункты меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("MENU", "Clicked MenuItem is " + item.getTitle());

        int id = item.getItemId();

        switch (id){
            case R.id.action_add_channel:
                Intent intent = new Intent(getApplicationContext(), ListViewChecked.class);
                startActivity(intent);
                break;
            case R.id.action_checked_channels:
                intent = new Intent(getApplicationContext(), CheckedChannelsActivity.class);
                startActivity(intent);
                break;
            case R.id.action_current_programs:
                intent = new Intent(getApplicationContext(), CurrentProgramActivity.class);
                startActivity(intent);
                break;
            case R.id.action_update:
                if (isOnline()) {
                    UpdateTask updateTask = new UpdateTask();
                    updateTask.execute();

                } else {
                    Toast.makeText(getApplicationContext(), "Нет интерета!", Toast.LENGTH_SHORT).show();

                }

                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public boolean onQueryTextChange(String text_new) {
        Log.d("QUERY", "New text is " + text_new);
        CheckedChannelsActivity.this.adapter.getFilter().filter(text_new); //фильтр адаптера списка
        return true;
    }




    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbAdapter.close();
    }

    @Override
    public void onClick(View v) {

    }

    private void reloadListView(){
        cursor = dbAdapter.getProgramLoad();
        channels = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int i = 0; i < channels.length; i++) {
            channels[i] = cursor.getString(cursor.getColumnIndex(DbHelper.CHANNEL_NAME));
            cursor.moveToNext();
        }
        cursor.close();

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, channels);

        listView.setAdapter(adapter);
    }

    public class UpdateTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Загрузка расписания", Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            //добавляем расписания выбранных каналов в бд
            String[][] tt;
            String channel;
            dbAdapter.deleteAllTT();
            cursor = dbAdapter.getProgramLoad();
            cursor.moveToFirst();
            Log.i("MyTag", Integer.toString(cursor.getCount()));
            for (int i = 0; i < cursor.getCount(); i++) {
                channel = cursor.getString(cursor.getColumnIndex(DbHelper.CHANNEL_NAME));
                tt = pars.get_all(channel);
                dbAdapter.updateCheckedChannels(channel, 2); //флаг загруженной программы
                Log.i("MyTag", cursor.getString(cursor.getColumnIndex(DbHelper.CHANNEL_NAME)));
                cursor.moveToNext();
                for (int j = 0; j < tt.length; j++) {
                    int k = 0;
                    if (tt[j][k] != null) {
                        dbAdapter.createProgram(tt[j][k], tt[j][k + 1], tt[j][k + 2], channel);
                    } else {
                        Log.i("MyTag", Integer.toString(i));
                        break;
                    }
                }
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //progressBar.setVisibility(View.INVISIBLE);
            if (cursor.getCount() < 1) {
                Toast.makeText(getApplicationContext(), "Вы ничего не выбрали", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Расписание добавлено в бд", Toast.LENGTH_SHORT).show();
            }
            cursor.close();

            //после загрузки расписания переходит к списку текущих програм на выбранных каналах
            Intent intent = new Intent(getApplicationContext(), CurrentProgramActivity.class);
            startActivity(intent);
        }

    }

    public boolean isOnline() {

        Runtime runtime = Runtime.getRuntime();
        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }
}
