package com.prism.bd_project;

import android.app.TabActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;

/**
 * Created by New on 18.05.2015.
 */
public class ProgramActivity extends TabActivity {
    final String TAG1 = "Tag 1";
    final String TAG2 = "Tag 2";
    final String TAG3 = "Tag 3";
    final String TAG4 = "Tag 4";
    final String TAG5 = "Tag 5";
    final String TAG6 = "Tag 6";
    final String TAG7 = "Tag 7";
    ListView listViewProgram;
    ArrayAdapter<String> adapter;
    Cursor cursor;
    DbAdapter dbAdapter;
    String[] channels;
    String channelName;
    Intent intent1, intent2, intent3, intent4, intent5, intent6, intent7;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.program_layout);

        channelName = getIntent().getStringExtra("channelName");
        Log.i("IntentProgram", channelName);
        intent1 = new Intent(getApplicationContext(), MondayActivity.class);
        intent1.putExtra("channelName", channelName);
        intent2 = new Intent(getApplicationContext(), TuesdayActivity.class);
        intent2.putExtra("channelName", channelName);
        intent3 = new Intent(getApplicationContext(), WednesdayActivity.class);
        intent3.putExtra("channelName", channelName);
        intent4 = new Intent(getApplicationContext(), ThursdayActivity.class);
        intent4.putExtra("channelName", channelName);
        intent5 = new Intent(getApplicationContext(), FridayActivity.class);
        intent5.putExtra("channelName", channelName);
        intent6 = new Intent(getApplicationContext(), SaturdayActivity.class);
        intent6.putExtra("channelName", channelName);
        intent7 = new Intent(getApplicationContext(), SundayActivity.class);
        intent7.putExtra("channelName", channelName);



        TabHost tabHost = getTabHost();

        TabHost.TabSpec tabSpec;


        tabSpec = tabHost.newTabSpec(TAG1);
        tabSpec.setContent(intent1);
        tabSpec.setIndicator("Пн\n" + DateFormatCustom.getOnlyDate(DateFormatCustom.getDate().get("Monday")));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(TAG2);
        tabSpec.setContent(intent2);
        tabSpec.setIndicator("Вт\n" + DateFormatCustom.getOnlyDate(DateFormatCustom.getDate().get("Tuesday")));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(TAG3);
        tabSpec.setContent(intent3);
        tabSpec.setIndicator("Ср \n" + DateFormatCustom.getOnlyDate(DateFormatCustom.getDate().get("Wednesday")));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(TAG4);
        tabSpec.setContent(intent4);
        tabSpec.setIndicator("Чт\n" + DateFormatCustom.getOnlyDate(DateFormatCustom.getDate().get("Thursday")));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(TAG5);
        tabSpec.setContent(intent5);
        tabSpec.setIndicator("Пт\n" + DateFormatCustom.getOnlyDate(DateFormatCustom.getDate().get("Friday")));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(TAG6);
        tabSpec.setContent(intent6);
        tabSpec.setIndicator("Сб\n" + DateFormatCustom.getOnlyDate(DateFormatCustom.getDate().get("Saturday")));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec(TAG7);
        tabSpec.setContent(intent7);
        tabSpec.setIndicator("Вс\n" + DateFormatCustom.getOnlyDate(DateFormatCustom.getDate().get("Sunday")));
        tabHost.addTab(tabSpec);

    }

}
