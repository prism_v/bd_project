package com.prism.bd_project;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Toast;

public class CurrentProgramActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {

    Cursor cursor;
    Cursor cursor2;
    DbAdapter dbAdapter;
    DbHelper dbHelper;
    String[] channels;
    parsing pars = new parsing();
    public String[][] all_channel_list;

    // названия каналов (групп)
    String[] groups;

    // названия телефонов (элементов)
//    String[] phonesHTC = new String[] {"Sensation", "Desire", "Wildfire", "Hero"};
//    String[] phonesSams = new String[] {"Galaxy S II", "Galaxy Nexus", "Wave"};
//    String[] phonesLG = new String[] {"Optimus", "Optimus Link", "Optimus Black", "Optimus One"};
    String[] all;


    // коллекция для групп
    ArrayList<Map<String, String>> groupData;

    // коллекция для элементов одной группы
    ArrayList<Map<String, String>> childDataItem;

    // общая коллекция для коллекций элементов
    ArrayList<ArrayList<Map<String, String>>> childData;
    // в итоге получится childData = ArrayList<childDataItem>

    // список аттрибутов группы или элемента
    Map<String, String> m;

    ExpandableListView elvMain;


    /**
     * Called when the activity is first created.
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_program_layout);

        //подключение к бд
        dbAdapter = new DbAdapter(getApplicationContext());
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        cursor = dbAdapter.getProgramLoad();
        channels = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int i = 0; i < channels.length; i++) {
            channels[i] = cursor.getString(cursor.getColumnIndex(DbHelper.CHANNEL_NAME));
            cursor.moveToNext();
        }


        groups = channels; //принимает список каналов, которые будут помещены в название категории


        // заполняем коллекцию групп из массива с названиями групп
        groupData = new ArrayList<Map<String, String>>();
        for (String group : groups) {
            // заполняем список аттрибутов для каждой группы
            m = new HashMap<String, String>();
            m.put("groupName", group); // название канала
            groupData.add(m);
        }

        // список аттрибутов групп для чтения
        String groupFrom[] = new String[]{"groupName"};
        // список ID view-элементов, в которые будет помещены аттрибуты групп
        int groupTo[] = new int[]{android.R.id.text1};


        // создаем коллекцию для коллекций элементов
        childData = new ArrayList<ArrayList<Map<String, String>>>();


        //экперемент/////////////////////////
        for (int i = 0; i < groups.length; i++) {
            createList(getElems(groups[i]));
        }


        /////////////////////////////////////

        // список аттрибутов элементов для чтения
        String childFrom[] = new String[]{"phoneName"};
        // список ID view-элементов, в которые будет помещены аттрибуты элементов
        int childTo[] = new int[]{android.R.id.text1};

        SimpleExpandableListAdapter adapter = new SimpleExpandableListAdapter(
                this,
                groupData,
                android.R.layout.simple_expandable_list_item_1,
                groupFrom,
                groupTo,
                childData,
                android.R.layout.simple_list_item_1,
                childFrom,
                childTo);

        elvMain = (ExpandableListView) findViewById(R.id.elvMain);
        elvMain.setAdapter(adapter);

        cursor.close();

        //разворачиваем все списки
        for (int i = 0; i < groups.length; i++) {
            elvMain.expandGroup(i);
        }

    }

    public void createList(String[] elems) {

        //функция для создания коллекции элементов группы
        childDataItem = new ArrayList<Map<String, String>>();
        // заполняем список аттрибутов для каждого элемента
        for (String phone : elems) {
            m = new HashMap<String, String>();
            m.put("phoneName", phone); // название телефона
            childDataItem.add(m);
        }
        // добавляем в коллекцию коллекций
        childData.add(childDataItem);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.setVisible(false);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("Поиск");
        searchView.setOnQueryTextListener((SearchView.OnQueryTextListener) this);

        setTitle("сейчас на ТВ");

        return true;
    }

    //обработка нажатий на пункты меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("MENU", "Clicked MenuItem is " + item.getTitle());

        int id = item.getItemId();

        switch (id) {
            case R.id.action_add_channel:
                Intent intent = new Intent(getApplicationContext(), ListViewChecked.class);
                startActivity(intent);
                break;
            case R.id.action_checked_channels:
                intent = new Intent(getApplicationContext(), CheckedChannelsActivity.class);
                startActivity(intent);
                break;
            case R.id.action_current_programs:
                intent = new Intent(getApplicationContext(), CurrentProgramActivity.class);
                startActivity(intent);
                break;
            case R.id.action_update:
                if (isOnline()) {
                    UpdateTask updateTask = new UpdateTask();
                    updateTask.execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Нет интерета!", Toast.LENGTH_SHORT).show();

                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    //возвращает текущие программы на выбраных каналах
    public String[] getElems(String channelName) {
        int i = 0;

        if (dbAdapter.getProgramByDate(channelName, DateFormatCustom.getCurrentDate()).getCount() > 0) {
            cursor = dbAdapter.getProgramByTime(channelName, DateFormatCustom.getCurrentDate(), DateFormatCustom.getNearTimeProgram(dbAdapter.getProgramByDate(channelName, DateFormatCustom.getCurrentDate()))[0]);
            if (DateFormatCustom.getNearTimeProgram(dbAdapter.getProgramByDate(channelName, DateFormatCustom.getCurrentDate())).length == 2) {
                System.out.println("длина 2");
                cursor2 = dbAdapter.getProgramByTime(channelName, DateFormatCustom.getCurrentDate(), DateFormatCustom.getNearTimeProgram(dbAdapter.getProgramByDate(channelName, DateFormatCustom.getCurrentDate()))[1]);
                i = 1;
            }
            all = new String[cursor.getCount() + i];
            cursor.moveToFirst();
            for (int j = 0; j < cursor.getCount(); j++) {
                all[j] = cursor.getString(cursor.getColumnIndex(DbHelper.PROGRAM_TIME)) + " " + cursor.getString(cursor.getColumnIndex(DbHelper.PROGRAM_NAME));
                cursor.moveToNext();
            }
            if (all.length == 2) {
                cursor2.moveToFirst();
                all[1] = cursor2.getString(cursor.getColumnIndex(DbHelper.PROGRAM_TIME)) + " " + cursor2.getString(cursor.getColumnIndex(DbHelper.PROGRAM_NAME));
            }
        } else {
            Log.i("Mytag", "База пуста");
            all = new String[]{"обновите расписание"};
        }

        return all;
    }

    public boolean onQueryTextChange(String text_new) {
        Log.d("QUERY", "New text is " + text_new);
        return true;
    }

    public boolean onQueryTextSubmit(String text) {
        Log.d("QUERY", "Search text is " + text);
        return true;
    }

    public class UpdateTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Загрузка расписания", Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            //добавляем расписания выбранных каналов в бд
            String[][] tt;
            String channel;
            dbAdapter.deleteAllTT();
            cursor = dbAdapter.getProgramLoad();
            cursor.moveToFirst();
            Log.i("MyTag", Integer.toString(cursor.getCount()));
            for (int i = 0; i < cursor.getCount(); i++) {
                channel = cursor.getString(cursor.getColumnIndex(dbHelper.CHANNEL_NAME));
                tt = pars.get_all(channel);
                dbAdapter.updateCheckedChannels(channel, 2); //флаг загруженной программы
                Log.i("MyTag", cursor.getString(cursor.getColumnIndex(dbHelper.CHANNEL_NAME)));
                cursor.moveToNext();
                for (int j = 0; j < tt.length; j++) {
                    int k = 0;
                    if (tt[j][k] != null) {
                        dbAdapter.createProgram(tt[j][k], tt[j][k + 1], tt[j][k + 2], channel);
                    } else {
                        Log.i("MyTag", Integer.toString(i));
                        break;
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //progressBar.setVisibility(View.INVISIBLE);
            if (cursor.getCount() < 1) {
                Toast.makeText(getApplicationContext(), "Вы ничего не выбрали", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Расписание добавлено в бд", Toast.LENGTH_SHORT).show();
            }
            cursor.close();

            //после загрузки расписания переходит к списку текущих програм на выбранных каналах
            Intent intent = new Intent(getApplicationContext(), CurrentProgramActivity.class);
            startActivity(intent);
        }
    }

    public boolean isOnline() {

        Runtime runtime = Runtime.getRuntime();
        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }
}



