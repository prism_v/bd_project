package com.prism.bd_project;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.sql.SQLException;


public class ListViewActivity  extends Activity{
    Cursor cursor;
    DbHelper dbHelper;
    DbAdapter dbAdapter;
    ListView listView;
    CheckBox checkBox;
    Button btn_addChannels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_layout);

        checkBox = (CheckBox) findViewById(R.id.checkbox);
        btn_addChannels = (Button) findViewById(R.id.btn_addChannels);
        checkBox = (CheckBox) findViewById(R.id.checkbox);


        dbAdapter = new DbAdapter(getApplicationContext());
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //вывод списка всех каналов на экран
    private void fillData(){
        cursor = dbAdapter.fetchAllChannels();

        String[] from = new String[]{dbHelper.CHANNEL_NAME};
        int[] to = new int[]{R.id.text};

        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(this,
                R.layout.item, cursor, from, to, 0);

        listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(simpleCursorAdapter);

    }

    public void getCheckedItem(View view){
       int check = checkBox.getId();
        Log.i("MyTag", Integer.toString(check));
    }

    @Override
    protected void onStart() {
        super.onStart();
        fillData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbAdapter.close();
    }
}
