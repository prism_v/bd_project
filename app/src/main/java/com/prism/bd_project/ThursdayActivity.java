package com.prism.bd_project;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.sql.SQLException;

/**
 * Created by New on 19.05.2015.
 */
public class ThursdayActivity extends Activity {

    private ArrayAdapter<String> adapter;
    ListView listViewThursday;
    Cursor cursor;
    DbAdapter dbAdapter;
    String[] channels;
    String channelName;
    String DayOfWeek;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.thursday_layout);

        listViewThursday = (ListView) findViewById(R.id.listViewThurday);

        dbAdapter = new DbAdapter(getApplicationContext());
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        channelName = getIntent().getStringExtra("channelName");
        DayOfWeek = DateFormatCustom.getDate().get("Thursday");
        Log.i("IntentMonday", channelName);
        cursor = dbAdapter.getProgramByDate(channelName, DayOfWeek);
        channels = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int i = 0; i < channels.length; i++) {
            channels[i] = cursor.getString(cursor.getColumnIndex(DbHelper.PROGRAM_TIME))
                    + "  " + cursor.getString(cursor.getColumnIndex(DbHelper.PROGRAM_NAME));
            cursor.moveToNext();
        }
        cursor.close();
        if(channels.length < 1){
            channels = new String[]{"На этот день нет расписания"};
        }

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, channels);
        listViewThursday.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbAdapter.close();
    }
}
