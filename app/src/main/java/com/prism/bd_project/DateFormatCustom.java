package com.prism.bd_project;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by New on 20.05.2015.
 */
public class DateFormatCustom {

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEEE", new Locale("ru"));
    private static Date currentDate;
    private static GregorianCalendar gc;
    private static HashMap<String, String> Dates;
    private static String[] DaysOfWeek = new String[]{"Monday", "Tuesday", "Wednesday", "Thursday",
            "Friday", "Saturday", "Sunday"};
    private static DbAdapter dbAdapter;

    private static GregorianCalendar getDateOfMonday() {
        currentDate = new Date();
        gc = new GregorianCalendar(currentDate.getYear(), currentDate.getMonth(),
                currentDate.getDate());
        switch (gc.get(Calendar.DAY_OF_WEEK)) {
            case 1:   //Ср
                gc.add(gc.DATE, -2);
                break;
            case 2:     //Чт
                gc.add(gc.DATE, -3);
                break;
            case 3:     //Пт
                gc.add(gc.DATE, -4);
                break;
            case 4:     //Сб
                gc.add(gc.DATE, -5);
                break;
            case 5:     //Вс
                gc.add(gc.DATE, -6);
                break;
            case 6:     //Пн
                break;
            case 7:     //Вт
                gc.add(gc.DATE, -1);
                break;
        }

        return gc;
    }

    public static HashMap<String, String> getDate() {
        Dates = new HashMap<>();
        gc = getDateOfMonday();
        String year;
        String month;
        String date;

        for (int i = 0; i < DaysOfWeek.length; i++) {

            year = Integer.toString(gc.get(Calendar.YEAR) + 1900);
            if (gc.get(Calendar.MONTH) < 10) {
                month = "0" + Integer.toString(gc.get(Calendar.MONTH) + 1);
            } else {
                month = Integer.toString(gc.get(Calendar.MONTH) + 1);
            }
            date = Integer.toString(gc.get(Calendar.DATE));

            Dates.put(DaysOfWeek[i], year + month + date);

            gc.add(gc.DATE, 1);
        }

        return Dates;
    }

    public static String[] getNearTimeProgram(Cursor cursor) {
        String programTime;
        String s1;
        String s2;
        String[] s3;
        int h;
        int m;
        int timeInMinutes;
        int realsize = 0;
        int[] nums = new int[cursor.getCount()];
        int[] nums2;
        int[] times = new int[1];
        times[0] = 60;
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int timeInMinutesCurrent = (hour * 60) + minute;


        for (int i = 0; i < cursor.getCount(); i++) {
            if (i == 0) {
                cursor.moveToFirst();
            }
            programTime = cursor.getString(cursor.getColumnIndex(DbHelper.PROGRAM_TIME)); //строка времени из бд
            h = Integer.parseInt(programTime.substring(0, 2));  //часы
            m = Integer.parseInt(programTime.substring(3, 5));  //минуты
            timeInMinutes = (h * 60) + m;  //время в минутах

            //nums[i] = timeInMinutes;  //добавляем в список

            if(h > 4){
                nums[i] = (h * 60) + m;
                realsize++;
            }

            if (i != cursor.getCount() - 1) {
                cursor.moveToNext();
            }

        }

        nums2 = new int[realsize];
        for (int i = 0; i < realsize; i++) {
            nums2[i] = nums[i];
        }
        nums = nums2;

        //ВЕЛОСИПЕДИЩЕЕЕЕЕЕЕЕЕЕЕ
        if (timeInMinutesCurrent <= nums[0]) {
            times = new int[1];
            times[0] = nums[0];
            System.out.println(1);
        }else if((timeInMinutesCurrent > nums[nums.length - 1])){
            times = new int[1];
            times[0] = nums[nums.length - 1];
            System.out.println(5);
        } else {
            for (int i = 0; i < nums.length; i++) {
                if ((i + 1) < nums.length) {
                    if ((nums[i] <= timeInMinutesCurrent) && (timeInMinutesCurrent <= nums[i + 1])) {
                        times = new int[2];
                        times[0] = nums[i];
                        times[1] = nums[i + 1];
                        System.out.println(4);

                        break;
                    }
                }
            }
            System.out.println(3);
        }
        int k;
        s3 = new String[times.length];
        for (int i = 0; i < times.length; i++) {
            k = times[i] / 60;
            if (k < 10) {
                s1 = "0" + Integer.toString(k) + ":";
            } else {
                s1 = Integer.toString(k) + ":";
            }

            if ((times[i] % 60) < 10) {
                s2 = "0" + Integer.toString(times[i] % 60);
            } else {
                s2 = Integer.toString(times[i] % 60);
            }
            s3[i] = s1 + s2;
        }

        return s3;
    }

    public static String getCurrentDate() {
        String date = "0";
        currentDate = new Date();
        gc = new GregorianCalendar(currentDate.getYear(), currentDate.getMonth(),
                currentDate.getDate());
        switch (gc.get(Calendar.DAY_OF_WEEK)) {
            case 1:   //Ср
                date = DateFormatCustom.getDate().get("Wednesday");
                break;
            case 2:     //Чт
                date = DateFormatCustom.getDate().get("Thursday");
                break;
            case 3:     //Пт
                date = DateFormatCustom.getDate().get("Friday");
                break;
            case 4:     //Сб
                date = DateFormatCustom.getDate().get("Saturday");
                break;
            case 5:     //Вс
                date = DateFormatCustom.getDate().get("Sunday");
                break;
            case 6:     //Пн
                date = DateFormatCustom.getDate().get("Monday");
                break;
            case 7:     //Вт
                date = DateFormatCustom.getDate().get("Tuesday");
                break;
        }

        return date;
    }

    public static String getPreviousDate(){
        String data;
        currentDate = new Date();
        gc = new GregorianCalendar(currentDate.getYear(), currentDate.getMonth(),
                currentDate.getDate());
        gc.add(gc.DATE, - 1);

        String year;
        String month;
        String date;

        year = Integer.toString(gc.get(Calendar.YEAR) + 1900);
        if (gc.get(Calendar.MONTH) < 10) {
            month = "0" + Integer.toString(gc.get(Calendar.MONTH) + 1);
        } else {
            month = Integer.toString(gc.get(Calendar.MONTH) + 1);
        }
        date = Integer.toString(gc.get(Calendar.DATE));

        return data = year + month + date;
    }

    //возвращает число месяца
    public static String getOnlyDate(String date){
        return date.substring(6);
    }

//    public static void NearTimeProgram(Cursor cursor, String channelName){
//
//        String programTime;
//        String s1;
//        String s2;
//        String[] s3;
//        int h;
//        int m;
//        int k = 0;
//        int timeInMinutes;
//        int[] nums, nums2;
//        int[] times = new int[1];
//        times[0] = 60;
//        Calendar calendar = Calendar.getInstance();
//        int hour = calendar.get(Calendar.HOUR_OF_DAY);
//        int minute = calendar.get(Calendar.MINUTE);
//        int timeInMinutesCurrent = (hour * 60) + minute;
//
//        nums = new int[cursor.getCount()];
//        for (int i = 0; i < cursor.getCount(); i++) {
//            if (i == 0) {
//                cursor.moveToFirst();
//            }
//            programTime = cursor.getString(cursor.getColumnIndex(DbHelper.PROGRAM_TIME)); //строка времени из бд
//            h = Integer.parseInt(programTime.substring(0, 2));  //часы
//            m = Integer.parseInt(programTime.substring(3, 5));  //минуты
//
//            //timeInMinutes = (h * 60) + m;  //время в минутах
//
//            if(h > 4){
//                nums[i] = (h * 60) + m;
//                k++;
//            }
//            if (i != cursor.getCount() - 1) {
//                cursor.moveToNext();
//            }
//        }
//
//        if(!(getCurrentDate().equals(getDate().get("Monday")))){
//            cursor = dbAdapter.getProgramByDate(channelName, getPreviousDate());
//
//            for (int i = 0; i < cursor.getCount(); i++) {
//                if(i == 0){cursor.moveToFirst();}
//
//
//            }
//
//        }
//
//    }
}