package com.prism.bd_project;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;

/**
 * Created by New on 09.05.2015.
 */
public class DbAdapter {

    private Context context;
    private SQLiteDatabase database;
    private DbHelper dbHelper;


    public DbAdapter(Context context){
        this.context = context;
    }

    public DbAdapter open() throws SQLException{
        dbHelper = new DbHelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        dbHelper.close();
    }

    //добавляет новый канал в общий список при парсинге
    //возвращает номер строки rowId
    public long createChannel(String channelName, String category, String link){
        ContentValues initialValues = createContentValues(channelName, category, link);

        return database.insert(DbHelper.TABLE_NAME, null, initialValues);
    }

    //добавляет программу выбранного канала
    public long createProgram(String programTime, String programName, String programDate, String channelName){
        ContentValues initialValues = timetableContentValues(programTime, programName, programDate, channelName);

        return database.insert(DbHelper.TABLE_TT, null, initialValues);
    }

    //обновить список выбраных каналов
    public boolean updateCheckedChannels(String channelName, int checkStatus){
        ContentValues updateValues = new ContentValues();
        updateValues.put(DbHelper.CHECKED_CHANNELS, checkStatus);

        return database.update(DbHelper.TABLE_NAME, updateValues, DbHelper.CHANNEL_NAME +
        "=" + "'" + channelName + "'", null) > 0;
    }

    //(возможно не надо)удалить эелемент списка
    public void deleteChannel(String channelName){
        ContentValues updateValues = new ContentValues();
        updateValues.put(DbHelper.CHECKED_CHANNELS, 0);
        database.update(DbHelper.TABLE_NAME, updateValues, DbHelper.CHANNEL_NAME + "=" + "'" + channelName + "'", null);
        database.delete(DbHelper.TABLE_TT, DbHelper.CHANNEL_NAME + "=" + "'" + channelName + "'", null);
    }

    public void deleteAllTT(){
        database.delete(DbHelper.TABLE_TT, null, null);
    }

    public void deleteAllChannels(){
        database.delete(DbHelper.TABLE_NAME, null, null);
    }


    //возвращает курсор со списком все каналов
    public Cursor fetchAllChannels(){
        return database.query(DbHelper.TABLE_NAME, new String[]{DbHelper._ID,
        DbHelper.CHANNEL_NAME, DbHelper.CATEGORY, DbHelper.LINK_ON_TIMETABLE,
                DbHelper.CHECKED_CHANNELS},
                null, null,  null, null, null);
    }

    //список выбраных каналов
    public Cursor getCheckedChannels(){
        return database.query(false, dbHelper.TABLE_NAME, new String[]{DbHelper.CHANNEL_NAME}, "(" + DbHelper.CHECKED_CHANNELS + "=" + 1 + ")", null, null, null, null, null);
    }

    //список выбранных каналов с загруженой программой
    public Cursor getProgramLoad(){
        return database.query(false, dbHelper.TABLE_NAME, new String[]{DbHelper.CHANNEL_NAME}, "(" + DbHelper.CHECKED_CHANNELS + "=" + 2 + ")", null, null, null, null, null);
    }

    //список выбранных каналов с загруженой программой
    public Cursor getUncheckedChannels(){
        return database.query(dbHelper.TABLE_NAME, new String[]{DbHelper.CHANNEL_NAME},DbHelper.CHECKED_CHANNELS + "=" + 0, null, null, null, null, null);
    }

    //список програм указанного канала
    public Cursor getProgramByDate(String channelName, String programDate){
        return database.query(dbHelper.TABLE_TT, new String[]{dbHelper.PROGRAM_TIME, dbHelper.PROGRAM_NAME},"(" +  dbHelper.CHANNEL_NAME + "=" + "'" + channelName + "'"
        + "AND " + dbHelper.PROGRAM_DATE + "=" + "'" + programDate + "'" + ")", null, null, null, null, null);
    }

    //передача определенного канала в указаное время
    public Cursor getProgramByTime(String channelName, String programDate, String programTime){
        return database.query(dbHelper.TABLE_TT, new String[]{dbHelper.PROGRAM_TIME, dbHelper.PROGRAM_NAME},"(" +   dbHelper.CHANNEL_NAME + "=" + "'" + channelName + "'"
                + "AND " + dbHelper.PROGRAM_DATE + "=" + "'" + programDate + "'" + "AND " + dbHelper.PROGRAM_TIME + "=" + "'" + programTime + "'" + ")", null, null, null, null, null);
    }

    public Cursor getCategories(){
        return database.query(true, DbHelper.TABLE_NAME, new String[]{DbHelper.CATEGORY}, null, null, null, null, null, null);
    }

    public Cursor getChannelsByCategory(String category){
        return database.query(false, dbHelper.TABLE_NAME, new String[]{DbHelper.CHANNEL_NAME}, "(" + DbHelper.CHECKED_CHANNELS + "=" + 0 +
               " AND " +  DbHelper.CATEGORY + "=" + "'" + category + "'" + ")", null, null, null, null, null);
    }



    private ContentValues createContentValues(String channelName, String category, String link){
        ContentValues values = new ContentValues();
        values.put(DbHelper.CHANNEL_NAME, channelName);
        values.put(DbHelper.CATEGORY, category);
        values.put(DbHelper.LINK_ON_TIMETABLE, link);
        values.put(DbHelper.CHECKED_CHANNELS, 0);
        return values;
    }

    private ContentValues timetableContentValues(String programTime, String programName, String programDate, String channelName){
        ContentValues values = new ContentValues();
        values.put(DbHelper.PROGRAM_TIME, programTime);
        values.put(DbHelper.PROGRAM_NAME, programName);
        values.put(DbHelper.PROGRAM_DATE, programDate);
        values.put(DbHelper.CHANNEL_NAME, channelName);
        return values;
    }
}
