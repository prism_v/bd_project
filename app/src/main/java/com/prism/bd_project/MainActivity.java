package com.prism.bd_project;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.support.v7.widget.SearchView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.support.v7.widget.SearchView.OnQueryTextListener;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Time;


public class MainActivity extends ActionBarActivity implements OnQueryTextListener {
    private DbAdapter dbAdapter;
    private DbHelper dbHelper;
    parsing pars = new parsing();
    public String[][] all_channel_list;
    MyTask myTask;
    private static final String TAG = "MyTag";
    Cursor cursor;
    Intent intent = new Intent();
    ProgressBar progressBar;

    private Button loadAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadAll = (Button) findViewById(R.id.btn_loadAll);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        progressBar.setVisibility(View.INVISIBLE);

        dbAdapter = new DbAdapter(getApplicationContext());
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(dbAdapter.fetchAllChannels().getCount() > 0){
            loadAll.setVisibility(View.INVISIBLE);
        }

        if(dbAdapter.fetchAllChannels().getCount() < 1){
            return;
        }else if(dbAdapter.getProgramLoad().getCount() > 0){
            Intent intent = new Intent(getApplicationContext(), CurrentProgramActivity.class);
            startActivity(intent);
        }else {
            Intent intent = new Intent(getApplicationContext(), ListViewChecked.class);
            startActivity(intent);
        }


//else if(dbAdapter.getCheckedChannels() != null){
//            Toast.makeText(getApplicationContext(), "Нужно загрузить расписание", Toast.LENGTH_LONG);
//        }

    }

    public void LoadChannels(View view) {
        //запуск первого парсинга во 2 потоке

        if(isOnline()){myTask = new MyTask();
            myTask.execute();
        }else {
            Toast.makeText(getApplicationContext(), "Нет интернета", Toast.LENGTH_SHORT).show();
        }


        pars.loadAll = false; //флаг окончания парсинга на 0
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.setVisible(false);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("Поиск");
        searchView.setOnQueryTextListener((SearchView.OnQueryTextListener) this);

        setTitle("ТВ программа");

        return true;
    }

    //обработка нажатий на пункты меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("MENU", "Clicked MenuItem is " + item.getTitle());

        int id = item.getItemId();

        switch (id){
            case R.id.action_add_channel:
                cursor = dbAdapter.fetchAllChannels();
                if(cursor.getCount() < 1){
                    Toast.makeText(getApplicationContext(), "Загрузите список каналов!", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(getApplicationContext(), ListViewChecked.class);
                    startActivity(intent);
                }
                break;
            case R.id.action_checked_channels:
                cursor = dbAdapter.fetchAllChannels();
                if(cursor.getCount() < 1){
                    Toast.makeText(getApplicationContext(), "Загрузите список каналов!", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(getApplicationContext(), CheckedChannelsActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.action_current_programs:
                cursor = dbAdapter.fetchAllChannels();
                if(cursor.getCount() < 1){
                    Toast.makeText(getApplicationContext(), "Загрузите список каналов!", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(getApplicationContext(), CurrentProgramActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.action_update:
                    Toast.makeText(getApplicationContext(), "Пока нечего обновлять =)", Toast.LENGTH_SHORT).show();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onQueryTextChange(String text_new) {
        Log.d("QUERY", "New text is " + text_new);
        return true;
    }

    public boolean onQueryTextSubmit(String text) {
        Log.d("QUERY", "Search text is " + text);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbAdapter != null) {
            dbAdapter.close();
        }
    }

    public class MyTask extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {

            dbAdapter.deleteAllChannels();
            all_channel_list = pars.all_channel_list();

            while (pars.loadAll == false) {Log.i(TAG, "загрузка каналов");}
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Загрузка списка каналов", Toast.LENGTH_LONG).show();
            loadAll.setClickable(false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //вставка инфы в базу
            for (int i = 0; i < all_channel_list.length; i++) {
                int j = 0;
                if (all_channel_list[i][j] != null) {
                    dbAdapter.createChannel(all_channel_list[i][j],
                            all_channel_list[i][j + 1],
                            all_channel_list[i][j + 2]);
                }else {
                    Log.i(TAG, Integer.toString(i));
                    break;
                }
            }

            Toast.makeText(getApplicationContext(), "Загрузка завершена", Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.INVISIBLE);
            //переход на ListViewChecked.class со списком всех каналов
            Intent intent = new Intent(getApplicationContext(), ListViewChecked.class);
            startActivity(intent);
            SystemClock.sleep(1000);
            loadAll.setClickable(true);
        }


    }


    public static boolean hasConnection(final Context context)
    {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected())
        {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected())
        {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        if (wifiInfo != null && wifiInfo.isConnected())
        {
            return true;
        }
        return false;
    }

    public boolean isOnline() {

        Runtime runtime = Runtime.getRuntime();
        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }
        //Toast.makeText(getApplicationContext(), "Нет инета!!!", Toast.LENGTH_SHORT).show();
        return false;
    }
}
