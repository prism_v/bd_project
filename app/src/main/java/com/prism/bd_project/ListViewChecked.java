package com.prism.bd_project;


import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;
import java.sql.SQLException;

public class ListViewChecked extends ActionBarActivity implements OnClickListener, SearchView.OnQueryTextListener {

    ListView listView;
    Cursor cursor;
    DbHelper dbHelper;
    DbAdapter dbAdapter;
    ArrayAdapter<String> adapter;
    String[] channels;
    parsing pars;
    String[][] all_program_list;
    int count = 0;
    ProgressBar progressBar;
    String[] data = {"one", "two", "three", "four", "five"};


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_checked);


        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        progressBar.setVisibility(View.INVISIBLE);

        dbAdapter = new DbAdapter(getApplicationContext());
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }



        //массив всех каналов
        dbHelper = new DbHelper(getApplicationContext());
        cursor = dbAdapter.getUncheckedChannels();
        channels = new String[cursor.getCount()];
        while (cursor.moveToNext()) {
            if(count == 0){cursor.moveToFirst();}
                channels[count] = cursor.getString(cursor.getColumnIndex(dbHelper.CHANNEL_NAME));
                count++;
        }
        cursor.close();





        listView = (ListView) findViewById(R.id.lvMain);
        // устанавливаем режим выбора пунктов списка
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        //создаем адаптер
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, channels);
        listView.setAdapter(adapter);

        Button btnChecked = (Button) findViewById(R.id.btnChecked);
        btnChecked.setOnClickListener(this);



        //////////////////////////////
        // адаптер
        cursor = dbAdapter.getCategories();
        count = 0;
        String[] Category = new String[cursor.getCount() + 1];
        while (cursor.moveToNext()) {
            if(count == 0){
                cursor.moveToFirst();
                Category[count] = "Все каналы";
                count++;
            }
            Category[count] = cursor.getString(cursor.getColumnIndex(dbHelper.CATEGORY));
            count++;
        }
        cursor.close();

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Category);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(adapter1);
        // заголовок
        spinner.setPrompt("Title");
        // выделяем элемент
        spinner.setSelection(0);
        // устанавливаем обработчик нажатия
        final Spinner spinner1 = spinner;
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {



                if(spinner1.getSelectedItem().toString().equals("Все каналы")){
                    cursor = dbAdapter.getUncheckedChannels();
                }else{
                    cursor = dbAdapter.getChannelsByCategory(spinner1.getSelectedItem().toString());
                }

                count = 0;
                channels = new String[cursor.getCount()];
                while (cursor.moveToNext()) {
                    if(count == 0){
                        cursor.moveToFirst();
                    }
                    channels[count] = cursor.getString(cursor.getColumnIndex(dbHelper.CHANNEL_NAME));
                    count++;
                }
                cursor.close();
                adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_multiple_choice, channels);
                listView.setAdapter(adapter);
                //adapter.notifyDataSetChanged();


            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        /////////////////////////
    }

    public void onClick(View view) {
        // пишем в лог выделенные элементы
        Log.i("MyTag", "checked: ");
        SparseBooleanArray sbArray = listView.getCheckedItemPositions();
        for (int i = 0; i < sbArray.size(); i++) {
            int key = sbArray.keyAt(i);
            if (sbArray.get(key)) {
                String ch = ListViewChecked.this.adapter.getItem(key);
                Log.i("MyTag", ch);
                dbAdapter.updateCheckedChannels(ch, 1);
            }
        }

        if(isOnline()) {
            TaskTT taskTT = new TaskTT();
            taskTT.execute();
        }else{
            Toast.makeText(getApplicationContext(), "Нет интернета!", Toast.LENGTH_SHORT).show();
        }
    }

    public void Click(View view){
//        Intent intent = new Intent(getApplicationContext(), CurrentProgramActivity.class);
//        startActivity(intent);
    }

    public void checkedChannels(View view){

        Intent intent = new Intent(this, CheckedChannelsActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("Поиск");
        searchView.setOnQueryTextListener((SearchView.OnQueryTextListener) this);

        setTitle("Добавить канал");

        return true;
    }

    //обработка нажатий на пункты меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("MENU", "Clicked MenuItem is " + item.getTitle());

        int id = item.getItemId();

        switch (id){
            case R.id.action_add_channel:
                Intent intent = new Intent(getApplicationContext(), ListViewChecked.class);
                startActivity(intent);
                break;
            case R.id.action_checked_channels:
                intent = new Intent(getApplicationContext(), CheckedChannelsActivity.class);
                startActivity(intent);
                break;
            case R.id.action_current_programs:
                intent = new Intent(getApplicationContext(), CurrentProgramActivity.class);
                startActivity(intent);
                break;
            case R.id.action_update:
                if (isOnline()) {
                    UpdateTask updateTask = new UpdateTask();
                    updateTask.execute();
                } else {
                    Toast.makeText(getApplicationContext(), "Нет интерета!", Toast.LENGTH_SHORT).show();

                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }


    public boolean onQueryTextChange(String text_new) {
        Log.d("QUERY", "New text is " + text_new);
        ListViewChecked.this.adapter.getFilter().filter(text_new); //фильтр адаптера списка
        return true;
    }

    public boolean onQueryTextSubmit(String text) {
        Log.d("QUERY", "Search text is " + text);
        ListViewChecked.this.adapter.getFilter().filter(text);  //фильтр адаптера списка
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbAdapter.close();
    }

    public class TaskTT extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Загрузка расписания", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            //добавляем расписания выбранных каналов в бд
            String[][] tt;
            String channel;
            cursor = dbAdapter.getCheckedChannels();
            cursor.moveToFirst();
            Log.i("MyTag", Integer.toString(cursor.getCount()));
            for (int i = 0; i <cursor.getCount(); i++) {
                channel = cursor.getString(cursor.getColumnIndex(dbHelper.CHANNEL_NAME));
                tt = pars.get_all(channel);
                dbAdapter.updateCheckedChannels(channel, 2); //флаг загруженной программы
                Log.i("MyTag", cursor.getString(cursor.getColumnIndex(dbHelper.CHANNEL_NAME)));
                cursor.moveToNext();
                for (int j = 0; j < tt.length; j++) {
                    int k = 0;
                    if (tt[j][k] != null) {
                        dbAdapter.createProgram(tt[j][k], tt[j][k + 1], tt[j][k + 2], channel);
                    }else {
                        Log.i("MyTag", Integer.toString(i));
                        break;
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressBar.setVisibility(View.INVISIBLE);
            if(cursor.getCount() < 1){
                Toast.makeText(getApplicationContext(), "Вы ничего не выбрали", Toast.LENGTH_SHORT).show();
            }else{
            Toast.makeText(getApplicationContext(), "Расписание добавлено в бд", Toast.LENGTH_SHORT).show();}
            cursor.close();

            //после загрузки расписания переходит к списку текущих програм на выбранных каналах
            Intent intent = new Intent(getApplicationContext(), CurrentProgramActivity.class);
            startActivity(intent);
        }
    }

    public class UpdateTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Загрузка расписания", Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            //добавляем расписания выбранных каналов в бд
            String[][] tt;
            String channel;
            dbAdapter.deleteAllTT();
            cursor = dbAdapter.getProgramLoad();
            cursor.moveToFirst();
            Log.i("MyTag", Integer.toString(cursor.getCount()));
            for (int i = 0; i < cursor.getCount(); i++) {
                channel = cursor.getString(cursor.getColumnIndex(DbHelper.CHANNEL_NAME));
                tt = pars.get_all(channel);
                dbAdapter.updateCheckedChannels(channel, 2); //флаг загруженной программы
                Log.i("MyTag", cursor.getString(cursor.getColumnIndex(DbHelper.CHANNEL_NAME)));
                cursor.moveToNext();
                for (int j = 0; j < tt.length; j++) {
                    int k = 0;
                    if (tt[j][k] != null) {
                        dbAdapter.createProgram(tt[j][k], tt[j][k + 1], tt[j][k + 2], channel);
                    } else {
                        Log.i("MyTag", Integer.toString(i));
                        break;
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //progressBar.setVisibility(View.INVISIBLE);
            if (cursor.getCount() < 1) {
                Toast.makeText(getApplicationContext(), "Вы ничего не выбрали", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Расписание добавлено в бд", Toast.LENGTH_SHORT).show();
            }
            cursor.close();

            //после загрузки расписания переходит к списку текущих програм на выбранных каналах
            Intent intent = new Intent(getApplicationContext(), CurrentProgramActivity.class);
            startActivity(intent);
        }
    }

    public boolean isOnline() {

        Runtime runtime = Runtime.getRuntime();
        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }
}
