package com.prism.bd_project;

import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;



/**
 * Created by SYSTEM on 03.05.2015.
 */




public class parsing {

    public static boolean loadAll = false;
    public static boolean loadTimetable = false;

    public static String[][] all_channel_list() {
        Document doc;
        String[][] result = new String[300][3];
        int i=0, j=0;
        try {
            String path = "http://tv.i.ua/channels/";
            doc = Jsoup.connect(path).get();

            Elements categories = doc.select("dt.clear");
            for (Element category : categories) {
                //System.out.println(category.text()); //Получаю все категории, на которые деляться каналы
                Elements channels = category.nextElementSibling().select("a[href]");
                //System.out.println(channels.size());

                for (Element channel : channels) {
                    //System.out.print(channel.text() + ",  "); //С помощью channel.text() получаю название каждого канала, для каждой категории
                    String link = "http://tv.i.ua" + channel.attr("href"); //Генерирую ссылку для преехода на страницу с расписанием для каждого канала

                    //Заполняю массив данными
                    result[i][j] = channel.text();
                    j++;
                    result[i][j] = category.text();
                    j++;
                    result[i][j] = link;
                    i++;
                    j=0;


                }


            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        loadAll = true;
        return result;

    }


    public static String[][]  get_all(String channel) {
        String[][] timetable_all = new String[1][3];
        int number=400;
        String[][] all_channels = all_channel_list();
        for (int i=0; i<300; i++) {
            if (all_channels[i][0].equals(channel)) {
                number = i;
                break;
            }
        }

        Document doc;
        try {
            doc = Jsoup.connect(all_channels[number][2]).get();
            Element something = doc.select("#sample").first().nextElementSibling();
            String some = something.toString();
            String time_and_name = some.substring(some.indexOf("':")+2, some.indexOf("var lens")-5);
            time_and_name = time_and_name.substring(time_and_name.indexOf("':")+2);

            int size = time_and_name.split("':'").length;
            String[] array = new String[size];
            array = time_and_name.split("':'");
            for (int i=0; i<size; i++) {
                if (array[i].contains("':")) {
                    int k = array[i].indexOf("':");
                    array[i] = array[i].substring(0, k - 9) + "," + array[i].substring(k + 3);
                }
                if (array[i].contains("'},")) {
                    int k = array[i].indexOf("'},");
                    array[i] = array[i].substring(0, k + 1) + "," + array[i].substring(k+4);
                }
                //  System.out.println(array[i]);
            }

            String[][] timetable = new String[size-1][3];
            timetable[0][0] = array[0].substring(array[0].indexOf("'")+1);

            for (int i=1; i<size-1; i++) {
                if (array[i].contains("','")) {
                    timetable[i-1][1] = array[i].split("','")[0];
                    timetable[i][0] = array[i].split("','")[1];
                }
            }

            timetable[size-2][1] = array[size-1].substring(0, array[size-1].length()-1);
            size--;

            for (int i=0; i<size; i++) {

                //Преобразую время передачи
                if (Integer.valueOf(timetable[i][0]) < 1000) {
                    timetable[i][0] = "0" + timetable[i][0];
                }
                if (timetable[i][0].charAt(0) == '3') {
                    timetable[i][0] = "0" + timetable[i][0].substring(1);
                }
                timetable[i][0] = timetable[i][0].substring(0, 2) + ":" + timetable[i][0].substring(2);

                //Нормальное отображение апострофа в названии телепрограммы
                if (timetable[i][1].contains("\'")) {
                    int q = timetable[i][1].indexOf("\'");
                    timetable[i][1] = timetable[i][1].substring(0,q-1) + "'" + timetable[i][1].substring(q+1);
                }

                //Если содержится ссылка перед названием программы
                if (timetable[i][1].contains("nofollow")) {
                    int q = timetable[i][1].indexOf("'+'>"),
                            w = timetable[i][1].indexOf("'/a'");
                    timetable[i][1] = timetable[i][1].substring(q+4,w-3);
                }
            }

            // Теперь разбираюсь с данными для третьей колонки(дата)

            String date = some.substring(some.indexOf("var lens"), some.indexOf("var mode")-5);
            date = date.substring(date.indexOf("':")+4);
            int amount = date.split(",'").length;
            String[] some_block = date.split(",'");
            String[][] block = new String[amount][2];
            for (int i=0; i<amount; i++) {
                block[i][0] = some_block[i].split("':")[0];
                block[i][1] = some_block[i].split("':")[1];
            }
            int counter=0;

            for (int i=0; i<amount; i++) {
                for (int j=counter; j<counter+Integer.valueOf(block[i][1]); j++) {
                    timetable[j][2] = block[i][0];
                }
                counter += Integer.valueOf(block[i][1]);
            }

            //Вывожу результат

//            for (int i=0; i<size; i++) {
//                for (int j=0; j<3; j++)
//                    System.out.print(timetable[i][j]+"|||");
//                System.out.println();
//            }
        timetable_all = timetable;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return timetable_all;
    }

    public static void main(String args[]) {}


}

