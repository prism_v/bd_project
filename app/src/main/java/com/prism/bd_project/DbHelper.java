package com.prism.bd_project;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Created by New on 05.05.2015.
 */
public class DbHelper extends SQLiteOpenHelper
implements BaseColumns{

    //константы для конструктора
    private static final String DATABASE_NAME =  "tv_database.db";
    private static final int DATABASE_VERSION =  1;
    public static final String TABLE_NAME = "allChannels_table";
    public static final String CHANNEL_NAME = "channelName";
    public static final String CATEGORY = "category";
    public static final String LINK_ON_TIMETABLE = "linkOnTimetable";
    public static final String CHECKED_CHANNELS = "checkedCHANNELS";

    public static final String TABLE_TT = "allTimetable";
    public static final String PROGRAM_TIME = "programTime";
    public static final String PROGRAM_NAME = "programName";
    public static final String PROGRAM_DATE = "programDate";
    //public static final String CHECKED_PROGRAMS = "checkedPrograms";

    private static final String TABLE_1 = "CREATE TABLE " + TABLE_NAME +
            " (" + DbHelper._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +  CHANNEL_NAME +
            " VARCHAR(255)," + CATEGORY + " VARCHAR(255)," + LINK_ON_TIMETABLE +
            " VARCHAR(255)," +  CHECKED_CHANNELS + " INTEGER(5)" + ");";

    private static final String TABLE_2 = "CREATE TABLE " + TABLE_TT + " (" +
            DbHelper._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + PROGRAM_TIME + " VARCHAR(255)," + PROGRAM_NAME + " VARCHAR(255),"
            + PROGRAM_DATE + " VARCHAR(255)," + CHANNEL_NAME + " VARCHAR(255)" + ");";

    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXIST " +
            TABLE_NAME;

    public DbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_1);
        db.execSQL(TABLE_2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w("LOG_TAG", "Обновление базы данных с версии " + oldVersion +
                " до версии " + newVersion + " ,которое удалит все старые данные");
        //удаляем предыдущую таблицу при апгрейде
        db.execSQL(TABLE_1);
        db.execSQL(TABLE_2);
        //создаем новый экземпляр таблицы
        onCreate(db);

    }
}
